# Client for Ubersmith API Exemple

This repository holds an exemple for connecting to Ubersmith API

## Demo

1. Copy .env.example to .env
2. Add credential to .env
3. execute composer install
4. execute demo.php

```
$ php code/demo.php
```
