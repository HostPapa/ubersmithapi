<?php
require __DIR__ . '/../vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;

class Ubersmith
{
    const STATUS_DEACTIVATED_ACCOUNT = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_PENDING = 2;
    const STATUS_SUSPENDED = 3;
    const STATUS_CANCELLED = 4;

    public function __construct()
    {
    }

    /**
     * Get a client
     *
     * @param int $client_id
     *
     * @return mixed
     */
    public function GetClient($client_id)
    {
        return $this->call("client.get", [
            'client_id' => $client_id,
            'metadata' => 1
        ]);
    }

    /**
     * Update a client
     *
     * @param int $client_id
     * @param array $params
     *
     * @return mixed
     */
    public function UpdateClient($client_id, $params = [])
    {
        $params['client_id'] = (int)$client_id;

        return $this->call("client.update", $params);
    }

    /**
     * List all service for a client
     *
     * @param int $client_id
     *
     * @return mixed
     */
    public function GetClientServices($client_id) {
        return $this->call("client.service_list", [
            'client_id' => $client_id,
            'options' => 1,
            'metadata' => 1,
            'pack_type_select' => 4
        ]);
    }

    /**
     * Get a service for a client
     *
     * @param int $service_id
     *
     * @return mixed
     */
    public function GetClientService($service_id)
    {
        return $this->call("client.service_get", [
            'service_id' => $service_id,
            'metadata' => 1
        ]);
    }

    /**
     * Update a service for a client
     *
     * @param int $service_id
     * @param array $params
     *
     * @return mixed
     */
    public function UpdateClientService($service_id, $params = [])
    {
        $params['service_id'] = (int)$service_id;

        if (isset($params["status"]) && ($params["status"] == 4) && !isset($params['auto_adjustment'])) {
            $params['auto_adjustment'] = 0;
        }

        return $this->call("client.service_update", $params);
    }

    /**
     * Main Call function to api endpoint
     *
     * @param string $method
     * @param array $params
     * @param string $dataType
     *
     * @return mixed
     */
    public function call($method, $params = null, $dataType = 'json')
    {
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => '*/*',
                getenv('UBERSMITH_ID') => getenv('UBERSMITH_KEY')
            ],
            'verify' => true,
            'decode_content' => true,
            'timeout' => 600
        ]);

        $stats = null;

        $onStats = function (TransferStats $s) use (&$stats) {
            $stats = $s->getHandlerStats();
        };

        $http_status_code = null;
        $response_content = null;

        if ($method == "uber.check_login") {
            $response = $client->get(getenv('UBERSMITH_URL'), [
                'query' => array_merge(['method' => $method], (array)$params),
                'on_stats' => $onStats
            ]);
        } elseif ($dataType == 'multipart') {
            $response = $client->post(getenv('UBERSMITH_URL'), [
                'query' => ['method' => $method],
                'multipart' => $params,
                'on_stats' => $onStats
            ]);

            $dataType = 'json'; // set the return type to json
        } else {
            $response = $client->post(getenv('UBERSMITH_URL'), [
                'query' => ['method' => $method],
                'body' => json_encode($params),
                'on_stats' => $onStats
            ]);
        }

        $http_status_code = $response->getStatusCode();
        $response_content = $response->getBody()->getContents();

        if ($http_status_code != 200) {
            print_r($response_content);
            die(500);
        }

        if ($dataType == 'json') {
            $response_content = json_decode($response_content, false);
        }

        return $response_content;
    }
}
